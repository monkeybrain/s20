/*arrow functions*/
// function sampleFunction(){
// 	alert(`Hello, I am filipino.`)
// };
// sampleFunction();

// const sampleFunction1 = () => {
// 	alert(`I am an arrow function`);
// };

// sampleFunction1();

// // implicit return with arrow function
// function prodNum(num1, num2){
// 	return num1 * num2
// };

// let prod = prodNum(25, 25);
// console.log(prod);

// // arrow function with implicit
// const prodNum1 = (num1, num2) => num1 * num2;
// let product = prodNum1(25, 10);
// console.log(product);

// const multiplyNum = (num1, num2) => {
// 	return num1*num2
// };

// let prod2 = multiplyNum(5, 10);
// console.log(prod2);

// function ans(a){
// 	if(a < 10){
// 		return 'true';
// 	} else {
// 		return 'false';
// 	}
// };

// let answer = ans(5)
// console.log(answer);

// ternary operator ?: - syntax: condition ? expression1 : expression2
/*const ans = (a) => (a < 10) ? 'true' : 'false';
let answer = ans(5)
console.log(answer);

let mark = prompt('Enter your grade: ');

let grade = (mark >= 75) ? 'Pass' : 'Fail'
console.log(`You ${grade} the exam.`);*/

/*
  convert the following function into an arrow function and display the result in console.

	let numbers = [1, 2, 3, 4, 5];
	
	let allValid = numbers.every(function(number){
		return (number < 3)
	});

	let filterValid = numbers.filter(function(number){
		return (number < 3)
	})
	
	let numberMap = numbers.map(function(number){
		return number * number
	})

*/

let numbers = [1, 2, 3, 4, 5];

const allValid = numbers.every(number => number < 3);

console.log(allValid);

const filterValid = numbers.filter(number => number < 3);
console.log(filterValid);

const numberMap = numbers.map(number => number * number );

console.log(numberMap);

// to check if number is positive, negative or zero
let num = 0;
let result = (num >= 0) ? (num == 0 ? 'zero' : 'positive') : 'negative';
console.log(`The number is ${result}`);

// JSON - JavaScript Object Notation
	// JSON is a string but formatted as JS Object
	// JSON is popularly used to pass data from one application to another
	// JSON is not only in JS bit also in other programming languages to pass data
	// this is why it is specified as JavaScript Object Notation
	// file extension .json
	// There is a way to turn JSON as JS Objects and there is way to turn JS Objects to JSON

// JS Object are not the same as JSON
	// JSON is a string
	// JS Object is an object
	// JSON keys are surrounded with double quotes.
	
	// Syntax:
	/*
		{
			"key1":"value1",
			"key2": "value1"
		}

	 - string
	 - number
	 - object
	 - array
	 - boolean
	 - null
	*/

const person = {
	"name" : "Juan",
	"weight": 175,
	"age" : 20,
	"eyeColor" : "brown",
	"cars" : ["Toyota", "Honda"],
	"favoriteBooks" : {
		"title" : "When the fire Nation Attack",
		"author" : "Nickeloden",
		"release" : "2021"
	}
};

console.log(typeof(person));

/*Mini-Activity
	Create an array of JSON format data with atleast 2 items
	Name the array as assets
	Each JSON format data should have the following key-value pairs:

		id - <value>
		name - <value>
		description - <value>
		isAvailable - <value>
		dateAdded - <value> 
*/

const assets = [{
		"id": "999",
		"name": "Angel",
		"description": "Good Entity",
		"isAvailable": true,
		"dateAdded": "12-25-2021AD"
},

{
		"id": "666",
		"name": "Debil",
		"description": "Bad Entity",
		"isAvailable": false,
		"dateAdded": "6/66/2266"
}];

console.log(assets);

// XML format - extensible markup language
/*
	<note>
		<to>Juan</to>
		<from>Maria Clara</from>
		<heading>Reminder</heading>
		<body>Don't forget me this weekend!</body>
	</note>
*/
// JSON - key value pairs - string
// {
// 	"to" : "Juan",
// 	"from" : "Maria Clara",
// 	"heading" : "Reminder",
// 	"body" : "don't forget me this weekend!"
// };

// let assets = [
// 	{
// 		"id" : "item-1",
// 		"name" : "Construction Crane",
// 		"description" : "Doesn't actually fly.",
// 		"isAvailable" : true,
// 		"dateAdded" : "July 7, 2018"
// 	},
// 	{
// 		"id" : "item-2",
// 		"name" : "Backhoe",
// 		"description" : "It has claw.",
// 		"isAvailable" : true,
// 		"dateAdded" : "July 14, 2018"
// 	},

// ];

// Stringify - method to convert JavaScript object to JSON and vice versa
// Converting JS Objects into JSON
	// this is commonly used when trying to pass data from one application to another via the use HTTP Request.
	// HTTP requests are requests fro resource between server and a client(browser)
	// JSON format is also in database

const cat = {
	"name":"Mashiro",
	"age" : 3,
	"weight" :20
}

console.log(cat);

const catJSON = JSON.stringify(cat);
console.log(catJSON);

// parse - reverse of using stringify
 const json = '{"name":"Mashiro","age":3,"weight":20}';
 const cat1 = JSON.parse(json);
 console.log(cat1);

/* let batchesArr = [
 	{
 		batchName : "Batch 131"
 	},
 	{
 		batchName : "Batch 132"
 	}
 ];
 console.log(JSON.stringify(batchesArr));

 let batchesJSON = `[
 	{
 		"batchName":"Batch 131"
 	},
 	{
 		"batchName":"Batch 132"
 	}
]`;*/

//console.log(JSON.parse(batchesJSON));

/*
	Create an arrow function called postCourse which allows us to add a new object into the array. It should receive data: id, name, description, price, isActive.

	Add the new course into the array and show the following alert:
		"You have created <nameOfCourse>. The price is <priceOfCourse>."

	Create an arrow function which allows us to find a particular course providing the course id and return the details of the found course.
		- use find()

	Create an arrow function called deleteCourse which can delete the last course object in the array.
		- pop()
 */

/*Pushing Instructions:

	Go to Gitlab:
		-in your zuitt-projects folder and access b131 folder.
		-inside your b131 folder create a new folder/subgroup: s20
		-inside s20, create a new project/repo called activity
		-untick the readme option
		-copy the git url from the clone button of your activity repo.

	Go to Gitbash:
		-go to your b131/s20 folder and access activity folder
		-initialize activity folder as a local repo: git init
		-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
		-add your updates to be committed: git add .
		-commit your changes to be pushed: git commit -m "includes JSON activity 1"
		-push your updates to your online repo: git push origin master

	Go to Boodle:
		-copy the url of the home page for your s20/activity repo (URL on browser not the URL from clone button) and link it to boodle:

		WD078-20 | Javascript - Introduction to JSON*/